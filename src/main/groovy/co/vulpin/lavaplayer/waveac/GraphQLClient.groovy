package co.vulpin.lavaplayer.waveac

import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.json.JSONObject
import org.jsoup.HttpStatusException

class GraphQLClient {

    private static final JSON_MIME = MediaType.parse("application/json")

    private final String url
    private final OkHttpClient httpClient = new OkHttpClient()

    GraphQLClient(String url) {
        this.url = url
    }

    JSONObject execute(String query, Map<String, Object> variables = [:]) {
        def jsonBody = new JSONObject([
            "query": query,
            "variables": variables
        ])

        def requestBody = RequestBody.create(JSON_MIME, jsonBody.toString())

        def req = new Request.Builder()
            .url(url)
            .post(requestBody)
            .build()

        def call = httpClient.newCall(req)

        def response = call.execute()

        if(response.code() == 200) {
            def responseBody = response.body().string()
            def json = new JSONObject(responseBody)
            return json
        } else {
            // Returns body as message because GraphQL returns detailed messages in
            // the body
            throw new HttpStatusException(response.body().string(), response.code(), req.url().toString())
        }
    }

}
