package co.vulpin.lavaplayer.waveac

import com.sedmelluq.discord.lavaplayer.container.ogg.OggAudioTrack
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManager
import com.sedmelluq.discord.lavaplayer.tools.io.PersistentHttpStream
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import com.sedmelluq.discord.lavaplayer.track.DelegatedAudioTrack
import com.sedmelluq.discord.lavaplayer.track.playback.LocalAudioTrackExecutor

class WaveAcAudioTrack extends DelegatedAudioTrack {

    private final WaveAcAudioSourceManager audioSourceManager

    final String username, permalink, privacyCode

    WaveAcAudioTrack(AudioTrackInfo trackInfo, String username, String permalink, String privacyCode, WaveAcAudioSourceManager audioSourceManager) {
        super(trackInfo)

        this.username = username
        this.permalink = permalink
        this.privacyCode = privacyCode

        this.audioSourceManager = audioSourceManager
    }

    @Override
    void process(LocalAudioTrackExecutor executor) throws Exception {
        def url = audioSourceManager.getStream(username, permalink, privacyCode)

        def stream = new PersistentHttpStream(audioSourceManager.interface, new URI(url), null)

        def delegate = new OggAudioTrack(info, stream)

        processDelegate(delegate, executor)
    }

    @Override
    AudioSourceManager getSourceManager() {
        return audioSourceManager
    }

    @Override
    boolean isSeekable() {
        return false
    }

    @Override
    AudioTrack makeClone() {
        return new WaveAcAudioTrack(info, username, permalink, privacyCode, audioSourceManager)
    }

}
