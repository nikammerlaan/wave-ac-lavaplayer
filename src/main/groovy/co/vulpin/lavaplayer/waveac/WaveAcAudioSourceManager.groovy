package co.vulpin.lavaplayer.waveac

import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManager
import com.sedmelluq.discord.lavaplayer.tools.DataFormatTools
import com.sedmelluq.discord.lavaplayer.tools.io.HttpClientTools
import com.sedmelluq.discord.lavaplayer.tools.io.HttpInterface
import com.sedmelluq.discord.lavaplayer.tools.io.HttpInterfaceManager
import com.sedmelluq.discord.lavaplayer.track.*
import org.json.JSONObject

class WaveAcAudioSourceManager implements AudioSourceManager {

    private static final ITEM_QUERY = '''
        query GetItem($username: String, $permalink: String, $privacyCode: String) {
            item(username: $username, permalink: $permalink, privacyCode: $privacyCode) {
                
                __typename
                
                ...on Track {
                    title
                    owner {
                        name
                    }
                    duration
                }
                
                ...on Playlist {
                    title
                }
                
           }
       }
    '''.stripMargin()

    private static final PLAYLIST_TRACKS_QUERY = '''
        query GetPlaylistTracks($username: String, $permalink: String, $privacyCode: String, $offset: Int) {
            playlist(username: $username, permalink: $permalink, privacyCode: $privacyCode) {
                tracks(offset: $offset) {
                    isNextPage 
                    tracks {
                        title
                        owner {
                            name
                            username
                        }
                        permalink
                        privacyCode
                        duration
                    }
                }
           }
       }
    '''.stripMargin()

    private static final TRACK_STREAM_QUERY = '''
        query GetTrackStream($username: String, $permalink: String, $privacyCode: String) {
            track(username: $username, permalink: $permalink, privacyCode: $privacyCode, filter: false) {
                stream(countPlay: false)
            }
       }
    '''.stripMargin()

    private static final BASE_ID_REGEX = "[a-z0-9-]+"
    private static final PRIVACY_CODE_REGEX = "[a-zA-Z0-9-_]+"
    private static final BASE_URL_REGEX = "(?:http(?:s)?:\\/\\/wave\\.ac\\/)"
    private static final ITEM_URL_REGEX = ~"$BASE_URL_REGEX(?<username>$BASE_ID_REGEX)\\/(?<permalink>$BASE_ID_REGEX)(?:\\/(?<privacyCode>$PRIVACY_CODE_REGEX))?(?:\\/)?"

    private static final API_URL = "https://api.wave.ac/graphql"

    private final HttpInterfaceManager httpInterfaceManager = HttpClientTools.createDefaultThreadLocalManager()
    private final GraphQLClient api = new GraphQLClient(API_URL)

    @Override
    String getSourceName() {
        return "wave.ac"
    }

    @Override
    AudioItem loadItem(DefaultAudioPlayerManager manager, AudioReference reference) {
        def input = reference.identifier

        def matcher = input =~ ITEM_URL_REGEX

        if(!matcher.matches())
            return null

        def username = matcher.group("username")
        def permalink = matcher.group("permalink")
        def privacyCode = matcher.group("privacyCode")

        return loadItem(username, permalink, privacyCode)
    }

    AudioItem loadItem(String username, String permalink, String privacyCode) {
        def response = api.execute(ITEM_QUERY, [
            username: username,
            permalink: permalink,
            privacyCode: privacyCode
        ])

        def item = response
            .getJSONObject("data")
            .getJSONObject("item")

        def type = item.getString("__typename")

        switch(type) {
            case "Track":
                return processTrack(item, username, permalink, privacyCode)
            case "Playlist":
                return processPlaylist(item, username, permalink, privacyCode)
            default:
                return AudioReference.NO_TRACK
        }
    }

    private AudioTrack processTrack(JSONObject json, String username, String permalink, String privacyCode) {
        def info = new AudioTrackInfo(
            json.getString("title"),
            json.getJSONObject("owner").getString("name"),
            json.getLong("duration") * 1000,
            username + "/" + permalink,
            false,
            getUrl(username, permalink, privacyCode)
        )

        return new WaveAcAudioTrack(info, username, permalink, privacyCode, this)
    }

    private AudioPlaylist processPlaylist(JSONObject playlistJson, String username, String permalink, String privacyCode) {
        List<AudioTrack> tracks = []

        def tracksJson
        def variables = [
            username: username,
            permalink: permalink,
            privacyCode: privacyCode,
            offset: 0
        ]

        do {
            tracksJson = api.execute(PLAYLIST_TRACKS_QUERY, variables)
                .getJSONObject("data")
                .getJSONObject("playlist")
            tracks += tracksJson
                .getJSONObject("tracks")
                .getJSONArray("tracks")
                .collect {
                    def json = it as JSONObject
                    def trackUsername = json.getJSONObject("owner").getString("username")
                    def trackPermalink = json.getString("permalink")
                    def trackPrivacyCode = json.optString("privacyCode")
                    return processTrack(json, trackUsername, trackPermalink, trackPrivacyCode)
                }
            variables.offset = tracks.size()
        } while(tracksJson.getJSONObject("tracks").getBoolean("isNextPage"))

        def playlistName = playlistJson.getString("title")

        return new BasicAudioPlaylist(playlistName, tracks, null, false)
    }

    protected String getStream(String username, String permalink, String privacyCode) {
        def result = api.execute(TRACK_STREAM_QUERY, [
            username: username,
            permalink: permalink,
            privacyCode: privacyCode
        ])

        return result
            .getJSONObject("data")
            .getJSONObject("track")
            .getString("stream")
    }

    protected HttpInterface getInterface() {
        return httpInterfaceManager.interface
    }

    @Override
    boolean isTrackEncodable(AudioTrack track) {
        return true
    }

    @Override
    void encodeTrack(AudioTrack track, DataOutput output) throws IOException {
        (track as WaveAcAudioTrack).with {
            output.writeUTF(username)
            output.writeUTF(permalink)
            DataFormatTools.writeNullableText(output, privacyCode)
        }
    }

    @Override
    AudioTrack decodeTrack(AudioTrackInfo trackInfo, DataInput input) throws IOException {
        def username = input.readUTF()
        def permalink = input.readUTF()
        def privacyCode = DataFormatTools.readNullableText(input)

        return new WaveAcAudioTrack(trackInfo, username, permalink, privacyCode, this)
    }

    @Override
    void shutdown() {
        httpInterfaceManager.close()
    }

    private static getUrl(String username, String permalink, String privacyCode) {
        def url = "https://wave.ac/$username/$permalink"
        if(privacyCode)
            url += "/" + privacyCode
        return url
    }

}
